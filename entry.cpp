/*
 * Copyright © 2024 Valve Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */


#include "entry.hpp"
#include <cstdio>
#include <string_view>

std::unordered_map<void*, InstanceDispatchTable> instanceTables;
std::unordered_map<void*, DeviceData> deviceData;
std::unordered_map<VkPhysicalDevice, VkInstance> deviceInstances;
std::mutex table_mutex = {};

extern "C" {

VKAPI_ATTR VkResult VKAPI_CALL ircheck_CreateInstance(const VkInstanceCreateInfo* pCreateInfo,
													  const VkAllocationCallbacks* pAllocator, VkInstance* pInstance) {
	VkLayerInstanceCreateInfo* layerCreateInfo = nullptr;
	for (const void* pNext = pCreateInfo->pNext; pNext;) {
		const VkBaseOutStructure* structure = reinterpret_cast<const VkBaseOutStructure*>(pNext);
		if (structure->sType == VK_STRUCTURE_TYPE_LOADER_INSTANCE_CREATE_INFO) {
			const VkLayerInstanceCreateInfo* info = reinterpret_cast<const VkLayerInstanceCreateInfo*>(structure);
			if (info->function == VK_LAYER_LINK_INFO)
				layerCreateInfo = const_cast<VkLayerInstanceCreateInfo*>(info);
		}
		pNext = structure->pNext;
	}
	PFN_vkGetInstanceProcAddr gipa = layerCreateInfo->u.pLayerInfo->pfnNextGetInstanceProcAddr;
	PFN_vkCreateInstance nextCreateInstance = reinterpret_cast<PFN_vkCreateInstance>(gipa(NULL, "vkCreateInstance"));

	layerCreateInfo->u.pLayerInfo = layerCreateInfo->u.pLayerInfo->pNext;
	VkResult result = nextCreateInstance(pCreateInfo, pAllocator, pInstance);
	if (result != VK_SUCCESS)
		return result;

	InstanceDispatchTable dispatchTable = { .nextGIPA = gipa,
											.enumeratePhysicalDevices =
												reinterpret_cast<PFN_vkEnumeratePhysicalDevices>(
													gipa(*pInstance, "vkEnumeratePhysicalDevices")) };
	std::lock_guard<std::mutex> lock = std::lock_guard<std::mutex>(table_mutex);
	instanceTables[getKey(*pInstance)] = dispatchTable;

	return VK_SUCCESS;
}

VKAPI_ATTR VkResult VKAPI_CALL ircheck_CreateDevice(VkPhysicalDevice physDevice, const VkDeviceCreateInfo* pCreateInfo,
													const VkAllocationCallbacks* pAllocator, VkDevice* pDevice) {
	VkLayerDeviceCreateInfo* layerCreateInfo = nullptr;
	for (const void* pNext = pCreateInfo->pNext; pNext;) {
		const VkBaseOutStructure* structure = reinterpret_cast<const VkBaseOutStructure*>(pNext);
		if (structure->sType == VK_STRUCTURE_TYPE_LOADER_DEVICE_CREATE_INFO) {
			const VkLayerDeviceCreateInfo* info = reinterpret_cast<const VkLayerDeviceCreateInfo*>(structure);
			if (info->function == VK_LAYER_LINK_INFO)
				layerCreateInfo = const_cast<VkLayerDeviceCreateInfo*>(info);
		}
		pNext = structure->pNext;
	}
	PFN_vkGetInstanceProcAddr gipa = layerCreateInfo->u.pLayerInfo->pfnNextGetInstanceProcAddr;
	PFN_vkGetDeviceProcAddr gdpa = layerCreateInfo->u.pLayerInfo->pfnNextGetDeviceProcAddr;
	PFN_vkCreateDevice nextCreateDevice = reinterpret_cast<PFN_vkCreateDevice>(gipa(NULL, "vkCreateDevice"));

	layerCreateInfo->u.pLayerInfo = layerCreateInfo->u.pLayerInfo->pNext;
	VkResult result = nextCreateDevice(physDevice, pCreateInfo, pAllocator, pDevice);
	if (result != VK_SUCCESS)
		return result;

	DeviceDispatchTable dispatchTable = {
		.nextGDPA = gdpa,
		.allocateMemory = reinterpret_cast<PFN_vkAllocateMemory>(gdpa(*pDevice, "vkAllocateMemory")),
		.mapMemory = reinterpret_cast<PFN_vkMapMemory>(gdpa(*pDevice, "vkMapMemory")),
		.unmapMemory = reinterpret_cast<PFN_vkUnmapMemory>(gdpa(*pDevice, "vkUnmapMemory")),
		.getBDA = reinterpret_cast<PFN_vkGetBufferDeviceAddress>(gdpa(*pDevice, "vkGetBufferDeviceAddress")),
		.getBDAKHR = reinterpret_cast<PFN_vkGetBufferDeviceAddressKHR>(gdpa(*pDevice, "vkGetBufferDeviceAddressKHR")),
		.getBDAEXT = reinterpret_cast<PFN_vkGetBufferDeviceAddressEXT>(gdpa(*pDevice, "vkGetBufferDeviceAddressEXT")),
		.createBuffer = reinterpret_cast<PFN_vkCreateBuffer>(gdpa(*pDevice, "vkCreateBuffer")),
		.destroyBuffer = reinterpret_cast<PFN_vkDestroyBuffer>(gdpa(*pDevice, "vkDestroyBuffer")),
		.freeMemory = reinterpret_cast<PFN_vkFreeMemory>(gdpa(*pDevice, "vkFreeMemory")),
		.copyBuffer = reinterpret_cast<PFN_vkCmdCopyBuffer>(gdpa(*pDevice, "vkCmdCopyBuffer")),
		.barrier = reinterpret_cast<PFN_vkCmdPipelineBarrier>(gdpa(*pDevice, "vkCmdPipelineBarrier")),
		.bindBufferMemory = reinterpret_cast<PFN_vkBindBufferMemory>(gdpa(*pDevice, "vkBindBufferMemory")),
		.bindBufferMemory2KHR = reinterpret_cast<PFN_vkBindBufferMemory2KHR>(gdpa(*pDevice, "vkBindBufferMemory2KHR")),
		.bindBufferMemory2 = reinterpret_cast<PFN_vkBindBufferMemory2>(gdpa(*pDevice, "vkBindBufferMemory2")),
		.getBuildSizes = reinterpret_cast<PFN_vkGetAccelerationStructureBuildSizesKHR>(
			gdpa(*pDevice, "vkGetAccelerationStructureBuildSizesKHR")),
		.cmdBuildKHR = reinterpret_cast<PFN_vkCmdBuildAccelerationStructuresKHR>(
			gdpa(*pDevice, "vkCmdBuildAccelerationStructuresKHR")),
		.queueSubmit = reinterpret_cast<PFN_vkQueueSubmit>(gdpa(*pDevice, "vkQueueSubmit")),
		.queueSubmit2 = reinterpret_cast<PFN_vkQueueSubmit2>(gdpa(*pDevice, "vkQueueSubmit2")),
		.queueSubmit2KHR = reinterpret_cast<PFN_vkQueueSubmit2KHR>(gdpa(*pDevice, "vkQueueSubmit2KHR")),
		.createFence = reinterpret_cast<PFN_vkCreateFence>(gdpa(*pDevice, "vkCreateFence")),
		.waitForFences = reinterpret_cast<PFN_vkWaitForFences>(gdpa(*pDevice, "vkWaitForFences")),
		.destroyFence = reinterpret_cast<PFN_vkDestroyFence>(gdpa(*pDevice, "vkDestroyFence"))
	};

	PFN_vkGetPhysicalDeviceMemoryProperties pfnMemProps = reinterpret_cast<PFN_vkGetPhysicalDeviceMemoryProperties>(
		gipa(deviceInstances[physDevice], "vkGetPhysicalDeviceMemoryProperties"));
	VkPhysicalDeviceMemoryProperties memProps;
	pfnMemProps(physDevice, &memProps);

	std::vector<VkMemoryType> types;
	types.reserve(memProps.memoryTypeCount);
	bool hasSetIndex = false;
	uint32_t hvvTypeIndex = 3;
	for (uint32_t i = 0; i < memProps.memoryTypeCount; ++i) {
		types.push_back(memProps.memoryTypes[i]);
		if (!hasSetIndex && (types[i].propertyFlags & (VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)) == (VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)) {
			hvvTypeIndex = i;
			hasSetIndex = true;
		}
	}
	fprintf(stderr, "hvv type index is %u\n", hvvTypeIndex);

	std::lock_guard<std::mutex> lock = std::lock_guard<std::mutex>(table_mutex);
	deviceData[getKey(*pDevice)] = DeviceData{
		.device = *pDevice, .dispatchTable = dispatchTable, .types = std::move(types), .hvvTypeIndex = hvvTypeIndex
	};

	return VK_SUCCESS;
}

VKAPI_ATTR PFN_vkVoidFunction VKAPI_CALL ircheck_GetDeviceProcAddr(VkDevice device, const char* _name) {
	std::string_view name = std::string_view(_name);
	if (name == "vkGetDeviceProcAddr") {
		return reinterpret_cast<PFN_vkVoidFunction>(ircheck_GetDeviceProcAddr);
	}
	if (name == "vkAllocateMemory") {
		return reinterpret_cast<PFN_vkVoidFunction>(ircheck_AllocateMemory);
	}
	if (name == "vkCreateBuffer") {
		return reinterpret_cast<PFN_vkVoidFunction>(ircheck_CreateBuffer);
	}
	if (name == "vkDestroyBuffer") {
		return reinterpret_cast<PFN_vkVoidFunction>(ircheck_DestroyBuffer);
	}
	if (name == "vkGetBufferDeviceAddress") {
		return reinterpret_cast<PFN_vkVoidFunction>(ircheck_GetBufferDeviceAddress);
	}
	if (name == "vkGetBufferDeviceAddressKHR") {
		return reinterpret_cast<PFN_vkVoidFunction>(ircheck_GetBufferDeviceAddressKHR);
	}
	if (name == "vkGetBufferDeviceAddressEXT") {
		return reinterpret_cast<PFN_vkVoidFunction>(ircheck_GetBufferDeviceAddressEXT);
	}
	if (name == "vkBindBufferMemory") {
		return reinterpret_cast<PFN_vkVoidFunction>(ircheck_BindBufferMemory);
	}
	if (name == "vkBindBufferMemory2KHR") {
		return reinterpret_cast<PFN_vkVoidFunction>(ircheck_BindBufferMemory2KHR);
	}
	if (name == "vkBindBufferMemory2") {
		return reinterpret_cast<PFN_vkVoidFunction>(ircheck_BindBufferMemory2);
	}
	if (name == "vkCmdBuildAccelerationStructuresKHR") {
		return reinterpret_cast<PFN_vkVoidFunction>(ircheck_CmdBuildAccelerationStructuresKHR);
	}
	if (name == "vkQueueSubmit") {
		return reinterpret_cast<PFN_vkVoidFunction>(ircheck_QueueSubmit);
	}
	if (name == "vkQueueSubmit2") {
		return reinterpret_cast<PFN_vkVoidFunction>(ircheck_QueueSubmit2);
	}
	if (name == "vkQueueSubmit2KHR") {
		return reinterpret_cast<PFN_vkVoidFunction>(ircheck_QueueSubmit2KHR);
	}
	std::lock_guard<std::mutex> lock = std::lock_guard<std::mutex>(table_mutex);
	return deviceData[getKey(device)].dispatchTable.nextGDPA(device, _name);
}

VKAPI_ATTR PFN_vkVoidFunction VKAPI_CALL ircheck_GetInstanceProcAddr(VkInstance instance, const char* _name) {
	std::string_view name = std::string_view(_name);
	if (name == "vkGetInstanceProcAddr") {
		return reinterpret_cast<PFN_vkVoidFunction>(ircheck_GetInstanceProcAddr);
	}
	if (name == "vkEnumeratePhysicalDevices") {
		return reinterpret_cast<PFN_vkVoidFunction>(ircheck_EnumeratePhysicalDevices);
	}
	if (name == "vkGetDeviceProcAddr") {
		return reinterpret_cast<PFN_vkVoidFunction>(ircheck_GetDeviceProcAddr);
	}
	if (name == "vkCreateInstance") {
		return reinterpret_cast<PFN_vkVoidFunction>(ircheck_CreateInstance);
	}
	if (name == "vkCreateDevice") {
		return reinterpret_cast<PFN_vkVoidFunction>(ircheck_CreateDevice);
	}
	std::lock_guard<std::mutex> lock = std::lock_guard<std::mutex>(table_mutex);
	return instanceTables[getKey(instance)].nextGIPA(instance, _name);
}

VKAPI_ATTR VkResult VKAPI_CALL vkNegotiateLoaderLayerInterfaceVersion(VkNegotiateLayerInterface* pVersionStruct) {
	pVersionStruct->loaderLayerInterfaceVersion = CURRENT_LOADER_LAYER_INTERFACE_VERSION;
	pVersionStruct->pfnGetInstanceProcAddr = ircheck_GetInstanceProcAddr;
	pVersionStruct->pfnGetDeviceProcAddr = ircheck_GetDeviceProcAddr;
	pVersionStruct->pfnGetPhysicalDeviceProcAddr = NULL;
	return VK_SUCCESS;
}
}
