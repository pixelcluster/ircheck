# ircheck
ircheck checks a RADV BVH intermediate BVH for correctness 
and if it can be safely converted using RADV's conversion shaders.

The following checks are implemented:
- Simple child offset validation (is the offset in bounds of the BVH and matches the node type?)
- Detection of unreferenced nodes and nodes with multiple references
- Detection of wrong child/parent ordering, capable of causing deadlocks in the internal converter.
