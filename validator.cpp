/*
 * Copyright © 2024 Valve Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "validator.hpp"
#include <bvh/build_interface.h>
#include <bvh/bvh.h>
#include <iostream>

Validator::Validator(uint8_t* scratch_data, const scratch_layout* layout, VkGeometryTypeKHR geometryType) {
	radv_ir_header* header = reinterpret_cast<radv_ir_header*>(scratch_data + layout->header_offset);

	m_bvh = BVH(header, scratch_data + layout->ir_offset, (layout->internal_node_offset - layout->ir_offset), layout->size - layout->ir_offset,
				geometryType);
}
bool Validator::validateBVH() {
	if (!m_bvh.m_isValid)
		return false;
	if (m_bvh.m_internalNodes.empty()) {
		fprintf(stderr, "internal nodes empty");
		return true;
	}
	bool result = !m_bvh.m_isIncomplete;
	/* Check child offsets for validity. */
	{
		m_bvh.foreachInternalTraverse([this, &result](auto& node, auto) {
			for (uint32_t child : node.children) {
				if (child == RADV_BVH_INVALID_NODE)
					continue;
				uint32_t child_offset = ir_id_to_offset(child);
				if (child_offset < m_bvh.m_internalNodeOffset) {
					if (ir_id_to_type(child) == radv_ir_node_internal) {
						std::cerr << "Child " << child << " is internal, but in leaf space!\n";
						result = false;
					}
				} else if (child_offset < m_bvh.m_size - sizeof(radv_ir_header)) {
					if (ir_id_to_type(child) != radv_ir_node_internal) {
						std::cerr << "Child " << child << " is leaf, but in internal space!\n";
						result = false;
					}
				} else {
					std::cerr << "Child " << child << " is invalid, but not INVALID_NODE!\n";
					result = false;
				}
			}
		});
	}

	/* Check whether all leaf nodes are referenced. If we got an incomplete BVH, this check will always fail. */
	if (!m_bvh.m_isIncomplete) {
		uint32_t leaf_node_size = 0;
		switch (m_bvh.m_geometryType) {
			case VK_GEOMETRY_TYPE_TRIANGLES_KHR:
				leaf_node_size = sizeof(radv_bvh_triangle_node);
				break;
			case VK_GEOMETRY_TYPE_AABBS_KHR:
				leaf_node_size = sizeof(radv_bvh_aabb_node);
				break;
			case VK_GEOMETRY_TYPE_INSTANCES_KHR:
				leaf_node_size = sizeof(radv_bvh_instance_node);
			default:
				break;
		}
		uint32_t actual_leaf_node_size = m_bvh.m_header.active_leaf_count * leaf_node_size;

		uint32_t referenced_leaf_node_size = 0;
		m_bvh.foreachInternalTraverse([&referenced_leaf_node_size, this](auto& node, auto) {
			for (uint32_t child : node.children) {
				if (child == RADV_BVH_INVALID_NODE)
					continue;
				uint32_t child_offset = ir_id_to_offset(child);
				if (child_offset < m_bvh.m_internalNodeOffset) {
					switch (ir_id_to_type(child)) {
						case radv_ir_node_triangle:
							referenced_leaf_node_size += sizeof(radv_bvh_triangle_node);
							break;
						case radv_ir_node_aabb:
							referenced_leaf_node_size += sizeof(radv_bvh_aabb_node);
							break;
						case radv_ir_node_instance:
							referenced_leaf_node_size += sizeof(radv_bvh_instance_node);
						default:
							break;
					}
				}
			}
		});

		if (actual_leaf_node_size != referenced_leaf_node_size) {
			std::cerr << "Actual leaf node size (" << actual_leaf_node_size << ") and referenced leaf node size ("
					  << referenced_leaf_node_size << ") do not match up!\n";
			result = false;
		}
	}

	/* Check for duplicate references */
	if (m_bvh.m_header.ir_internal_node_count > 0) {
		std::vector<std::vector<uint32_t>> leafReferences;
		std::vector<std::vector<uint32_t>> internalReferences;

		leafReferences.resize(m_bvh.m_leaves.size(), {});
		internalReferences.resize(m_bvh.m_header.ir_internal_node_count, {});
		internalReferences[m_bvh.m_header.ir_internal_node_count - 1].resize(1, ~0U);

		m_bvh.foreachInternalTraverse([&leafReferences, &internalReferences, this](auto& node, auto index) {
			for (uint32_t child : node.children) {
				if (child == RADV_BVH_INVALID_NODE)
					continue;
				uint32_t childOffset = ir_id_to_offset(child);
				if (childOffset < m_bvh.m_internalNodeOffset) {
					radv_ir_node* childPointer = reinterpret_cast<radv_ir_node*>(m_bvh.m_leafStorage + childOffset);
					uint32_t childIndex = ~0U;
					for (uint32_t i = 0; i < m_bvh.m_leaves.size(); ++i)
						if (m_bvh.m_leaves[i].node == childPointer) {
							childIndex = i;
							break;
						}
					if (childIndex < leafReferences.size())
						leafReferences[childIndex].push_back(index);
				} else if (childOffset < m_bvh.m_size - sizeof(radv_ir_header) &&
						   ((childOffset - m_bvh.m_internalNodeOffset) % sizeof(radv_ir_box_node)) == 0) {
					uint32_t childIndex = (childOffset - m_bvh.m_internalNodeOffset) / sizeof(radv_ir_box_node);
					if (childIndex < internalReferences.size()) {
						internalReferences[childIndex].push_back(index);
					}
				}
			}
		});

		uint32_t index = 0;
		for (auto& references : leafReferences) {
			if (references.size() > 1) {
				std::cerr << "Leaf node (index " << index
						  << ") is referenced by more than one node! Referencing internal indices:";
				for (auto& reference : references) {
					std::cerr << " " << reference;
				}
				std::cerr << "\n";
				result = false;
			}
			++index;
		}
		index = 0;
		for (auto& references : internalReferences) {
			if (references.size() == 0 && !m_bvh.m_isIncomplete) {
				std::cerr << "Unreferenced internal node at index " << index << "!\n";
				result = false;
			} else if (references.size() > 1) {
				std::cerr << "Internal node (index " << index
						  << ") is referenced by more than one node! Referencing internal indices:";
				for (auto& reference : references) {
					std::cerr << " " << reference;
				}
				std::cerr << "\n";
				result = false;
			}
			++index;
		}
	}

	/* Validate that children always come before their parents */
	m_bvh.foreachInternalTraverse([this, &result](auto& node, auto index) {
		uint32_t offset = m_bvh.m_internalNodeOffset + index * sizeof(radv_ir_box_node);
		for (uint32_t child : node.children) {
			if (child == RADV_BVH_INVALID_NODE)
				continue;
			uint32_t child_offset = ir_id_to_offset(child);
			if (child_offset > offset) {
				std::cerr << "Child " << child << " is located after internal node " << index
						  << "! This will deadlock!\n";
				result = false;
			}
		}
	});
	return result;
}
bool Validator::validateKeyIDs() { return true; }
bool Validator::validatePartitions() { return true; }
