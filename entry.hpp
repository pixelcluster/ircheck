/*
 * Copyright © 2024 Valve Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */


#ifndef IRCHECK_ENTRY_HPP
#define IRCHECK_ENTRY_HPP

#include <mutex>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <vulkan/vk_layer.h>


struct scratch_layout {
	uint32_t size;

	uint32_t header_offset;

	uint32_t sort_buffer_offset[2];
	uint32_t sort_internal_offset;

	uint32_t ploc_prefix_sum_partition_offset;
	uint32_t lbvh_node_offset;

	uint32_t ir_offset;
	uint32_t internal_node_offset;
};

// yoinked from baldurk's sample layer
template <typename DispatchableType> void* getKey(DispatchableType inst) { return *(void**)inst; }

struct InstanceDispatchTable {
	PFN_vkGetInstanceProcAddr nextGIPA;
	PFN_vkEnumeratePhysicalDevices enumeratePhysicalDevices;
};

struct DeviceDispatchTable {
	PFN_vkGetDeviceProcAddr nextGDPA;
	PFN_vkAllocateMemory allocateMemory;
	PFN_vkMapMemory mapMemory;
	PFN_vkUnmapMemory unmapMemory;
	PFN_vkGetBufferDeviceAddress getBDA;
	PFN_vkGetBufferDeviceAddressKHR getBDAKHR;
	PFN_vkGetBufferDeviceAddressEXT getBDAEXT;

	PFN_vkCreateBuffer createBuffer;
	PFN_vkDestroyBuffer destroyBuffer;
	PFN_vkFreeMemory freeMemory;
	PFN_vkCmdCopyBuffer copyBuffer;

	PFN_vkCmdPipelineBarrier barrier;

	PFN_vkBindBufferMemory bindBufferMemory;
	PFN_vkBindBufferMemory2KHR bindBufferMemory2KHR;
	PFN_vkBindBufferMemory2 bindBufferMemory2;

	PFN_vkGetAccelerationStructureBuildSizesKHR getBuildSizes;

	PFN_vkCmdBuildAccelerationStructuresKHR cmdBuildKHR;
	PFN_vkQueueSubmit queueSubmit;
	PFN_vkQueueSubmit2 queueSubmit2;
	PFN_vkQueueSubmit2KHR queueSubmit2KHR;

	PFN_vkCreateFence createFence;
	PFN_vkWaitForFences waitForFences;
	PFN_vkDestroyFence destroyFence;
};

struct BufferBindRange {
	uint64_t start;
	uint64_t size;
	VkBuffer buffer;
};

struct VARange {
	uint64_t start;
	uint64_t end;
	uint64_t size;
	std::vector<BufferBindRange> range;
};

struct BufferBindInfo {
	VkDeviceMemory memory;
	uint64_t offset;
};

struct BuildCommand {
	scratch_layout layout;
	void* scratchMem;
	VkBuffer buffer;
	VkDeviceMemory devmem;
	uint64_t leafCount;
	uint64_t scratchSize;
	VkAccelerationStructureTypeKHR structureType;
	VkGeometryTypeKHR geometryType;
	uint64_t scratchVA;
};

struct DeviceData {
	VkDevice device;
	struct DeviceDispatchTable dispatchTable;
	std::unordered_map<VkDeviceMemory, VARange> memoryMaps;
	std::unordered_map<VkBuffer, BufferBindInfo> bindInfos;
	std::unordered_map<VkBuffer, uint64_t> bufferSizes;
	std::unordered_map<VkCommandBuffer, std::vector<BuildCommand>> buildCommandBuffers;
	std::vector<VkMemoryType> types;
	uint32_t hvvTypeIndex;
};

extern std::unordered_map<void*, InstanceDispatchTable> instanceTables;
extern std::unordered_map<void*, DeviceData> deviceData;
extern std::unordered_map<VkPhysicalDevice, VkInstance> deviceInstances;
extern std::mutex table_mutex;

extern "C" {
VKAPI_ATTR VkResult VKAPI_CALL ircheck_EnumeratePhysicalDevices(VkInstance instance, uint32_t* pPhysicalDeviceCount,
																VkPhysicalDevice* pPhysicalDevices);
VKAPI_ATTR VkResult VKAPI_CALL ircheck_AllocateMemory(VkDevice device, const VkMemoryAllocateInfo* pAllocateInfo,
													  const VkAllocationCallbacks* pAllocator, VkDeviceMemory* pMemory);
VKAPI_ATTR VkDeviceAddress VKAPI_CALL ircheck_GetBufferDeviceAddress(VkDevice device,
																	 const VkBufferDeviceAddressInfo* pInfo);
VKAPI_ATTR VkDeviceAddress VKAPI_CALL ircheck_GetBufferDeviceAddressKHR(VkDevice device,
																		const VkBufferDeviceAddressInfoKHR* pInfo);
VKAPI_ATTR VkDeviceAddress VKAPI_CALL ircheck_GetBufferDeviceAddressEXT(VkDevice device,
																		const VkBufferDeviceAddressInfoEXT* pInfo);
VKAPI_ATTR VkResult VKAPI_CALL ircheck_CreateBuffer(VkDevice device, const VkBufferCreateInfo* pCreateInfo,
													const VkAllocationCallbacks* pAllocator, VkBuffer* pBuffer);
VKAPI_ATTR VkResult VKAPI_CALL ircheck_BindBufferMemory(VkDevice device, VkBuffer buffer, VkDeviceMemory memory,
														VkDeviceSize memoryOffset);
VKAPI_ATTR VkResult VKAPI_CALL ircheck_BindBufferMemory2KHR(VkDevice device, uint32_t bindInfoCount,
															const VkBindBufferMemoryInfoKHR* pBindInfos);
VKAPI_ATTR VkResult VKAPI_CALL ircheck_BindBufferMemory2(VkDevice device, uint32_t bindInfoCount,
														 const VkBindBufferMemoryInfo* pBindInfos);
VKAPI_ATTR void VKAPI_CALL ircheck_DestroyBuffer(VkDevice device, VkBuffer buffer,
												 const VkAllocationCallbacks* pAllocator);
VKAPI_ATTR void VKAPI_CALL ircheck_CmdBuildAccelerationStructuresKHR(
	VkCommandBuffer buffer, uint32_t infoCount, const VkAccelerationStructureBuildGeometryInfoKHR* pInfos,
	const VkAccelerationStructureBuildRangeInfoKHR* const* ppBuildRangeInfos);
VKAPI_ATTR VkResult VKAPI_CALL ircheck_QueueSubmit(VkQueue queue, uint32_t submitCount, const VkSubmitInfo* pSubmits,
												   VkFence fence);
VKAPI_ATTR VkResult VKAPI_CALL ircheck_QueueSubmit2(VkQueue queue, uint32_t submitCount, const VkSubmitInfo2* pSubmits,
													VkFence fence);
VKAPI_ATTR VkResult VKAPI_CALL ircheck_QueueSubmit2KHR(VkQueue queue, uint32_t submitCount,
													   const VkSubmitInfo2KHR* pSubmits, VkFence fence);
};
#endif // IRCHECK_ENTRY_HPP
