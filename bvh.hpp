/*
 * Copyright © 2024 Valve Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef IRCHECK_VALIDATION_HPP
#define IRCHECK_VALIDATION_HPP

#include <bvh/bvh.h>
#include <cstring>
#include <exception>
#include <fstream>
#include <iostream>
#include <optional>
#include <vector>

inline uint32_t ir_id_to_offset(uint32_t id) { return id & (~3U); }
inline uint32_t ir_id_to_type(uint32_t id) { return id & 3U; }

class BVH;

template <typename T>
concept TraversalFunctor = requires(T functor, radv_ir_box_node& node, uint32_t index) {
							   { functor(node, index) };
						   };

struct LeafNode {
	radv_ir_node* node;
	uint32_t type;
};

class BVH {
  public:
	BVH() {}
	BVH(radv_ir_header* header, uint8_t* bvh, uint32_t internal_node_offset, uint32_t asSize, VkGeometryTypeKHR geometryType)
		: m_size(asSize), m_geometryType(geometryType) {
		m_header = *header;
		bool unexpected_count = m_header.ir_internal_node_count > std::max(m_header.active_leaf_count, 2U) - 1 && false;
		if (unexpected_count) {
			std::cerr << "Internal node count " << m_header.ir_internal_node_count << " doesn't match expected "
					  << std::max(m_header.active_leaf_count, 2U) - 1 << " (based on leaf count).\n";
		}
		uint32_t internalNodeCount = m_header.ir_internal_node_count;
		if (static_cast<uint64_t>(internalNodeCount) * static_cast<uint64_t>(sizeof(struct radv_ir_box_node)) >
			static_cast<uint64_t>(asSize)) {
			std::cerr << "Internal node count " << internalNodeCount << " is invalid!\n";
			m_isValid = false;
		} else
			m_isValid = true;
		m_internalNodeOffset = internal_node_offset;
		m_leafStorage = new uint8_t[m_internalNodeOffset];

		std::memcpy(reinterpret_cast<char*>(m_leafStorage), bvh, m_internalNodeOffset);
		uint32_t offset = m_internalNodeOffset;
		if (m_isValid) {
			for (uint32_t i = 0; i < internalNodeCount; ++i, offset += sizeof(radv_ir_box_node)) {
				m_internalNodes.push_back(*reinterpret_cast<radv_ir_box_node*>(bvh + offset));
			}
		}
		add_leaf_nodes(m_header.ir_internal_node_count - 1);
	}
	BVH(BVH&) = delete;
	BVH& operator=(BVH&) = delete;
	BVH(BVH&&) = default;
	BVH& operator=(BVH&&) = default;

	uint8_t* m_leafStorage = nullptr;
	std::vector<LeafNode> m_leaves;
	std::vector<radv_ir_box_node> m_internalNodes;
	struct radv_ir_header m_header;
	uint32_t m_size;
	uint32_t m_internalNodeOffset;
	VkGeometryTypeKHR m_geometryType;
	bool m_isValid;
	/* Trigger fallback paths for broken internal node tree structure. Currently unused */
	bool m_isIncomplete = false;

	template <TraversalFunctor T> void foreachInternalTraverse(T functor) {
		if (m_isIncomplete) {
			uint32_t index = 0;
			for (auto& node : m_internalNodes) {
				functor(node, index);
				++index;
			}
		} else {
			uint32_t depth = 0;
			traverseInternalImpl(m_header.ir_internal_node_count - 1, functor, depth);
		}
	}

  private:
	template <TraversalFunctor T> void traverseInternalImpl(uint32_t index, T functor, uint32_t& depth) {
		auto& node = m_internalNodes[index];
		functor(node, index);
		++depth;

		if (depth > 1000) {
			std::cerr << "Reached 1000 depth, aborting!\n";
			abort();
		}
		for (uint32_t child : node.children) {
			if (child == RADV_BVH_INVALID_NODE)
				continue;
			uint32_t offset = ir_id_to_offset(child);
			if (offset >= m_internalNodeOffset && offset < m_size &&
				(offset - m_internalNodeOffset) % sizeof(radv_ir_box_node) == 0) {
				traverseInternalImpl((offset - m_internalNodeOffset) / sizeof(radv_ir_box_node), functor, depth);
			}
		}
		--depth;
	}

	void add_leaf_nodes(uint32_t index) {
		foreachInternalTraverse([this](auto& node, auto) {
			for (uint32_t child : node.children) {
				if (child == RADV_BVH_INVALID_NODE)
					continue;
				uint32_t offset = ir_id_to_offset(child);
				if (offset < m_internalNodeOffset) {
					m_leaves.push_back({ .node = reinterpret_cast<radv_ir_node*>(m_leafStorage + offset),
										 .type = ir_id_to_type(child) });
				}
			}
		});
	}
};

#endif // IRCHECK_VALIDATION_HPP
