/*
 * Copyright © 2024 Valve Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "validator.hpp"
#include <fstream>
#include <iostream>
#include <limits>

uint8_t *read_file(char *filename, size_t *size) {
	auto stream = std::ifstream(filename, std::ifstream::ate | std::ifstream::binary);
	if (!stream.is_open()) {
		std::cerr << "File " << filename << " not found!\n";
		exit(-1);
	}

	*size = stream.tellg();
	stream.seekg(std::ios_base::beg);
	stream.clear();

	uint8_t *data = new uint8_t[*size];
	stream.read(reinterpret_cast<char *>(data), *size);
	return data;
}

int main(int argc, char** argv) {
	if (argc <= 1) {
		std::cout << "Enter a filename to analyze\n";
		exit(-1);
	}

	size_t size;
	uint8_t *data = read_file(argv[1], &size);

	scratch_layout *layout = reinterpret_cast<scratch_layout *>(data + sizeof(VkGeometryTypeKHR));

	auto validator = Validator(data + sizeof(scratch_layout) + sizeof(VkGeometryTypeKHR),
							   layout,
								*reinterpret_cast<VkGeometryTypeKHR *>(data));
	if (validator.validateBVH() && validator.validateKeyIDs() && validator.validatePartitions())
		return 0;
	std::cerr
		<< "Invalid BVH detected. Relevant offsets:"
		<< "\nHeader at " << layout->header_offset
		<< "\nSrc ids at " << layout->sort_buffer_offset[0]
		<< "\nDst ids at " << layout->sort_buffer_offset[1]
		<< "\nSync state at " << layout->sort_internal_offset
		<< "\nIR at " << layout->ir_offset << "\n";
	return 1;
}
