/*
 * Copyright © 2024 Valve Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */


#include "bvh/build_interface.h"
#include <cstring>
#include <exception>
#include <execution>
#include <fstream>
#include <iostream>
#include <optional>
#include <vector>

struct key_id_pair {
	uint32_t id;
	uint32_t key;
};

template <typename T> std::optional<T> read(std::ifstream& stream) {
	T t;
	char data[sizeof(T)];
	stream.read(data, sizeof(T));
	if (stream.good()) {
		std::memcpy(&t, data, sizeof(T));
		return t;
	}
	return {};
}

int main(int argc, char** argv) {
	if (argc <= 1) {
		std::cout << "Enter a filename to analyze\n";
		exit(-1);
	}
	std::ifstream file = std::ifstream(argv[1]);
	uint32_t asCount = *read<uint32_t>(file);

	std::vector<uint32_t> asSizes;
	asSizes.reserve(asCount);
	for (uint32_t i = 0; i < asCount; ++i) {
		asSizes.push_back(*read<uint32_t>(file));
	}

	std::vector<key_id_pair> pairs;
	for (uint32_t i = 0; i < asCount; ++i) {
		uint64_t size = *read<uint64_t>(file);
		pairs.reserve(size / sizeof(key_id_pair));
		for (uint64_t j = 0; j < size / sizeof(key_id_pair); ++j) {
			pairs.push_back(*read<key_id_pair>(file));
		}

		uint64_t current_prefix_sum = 0;
		uint64_t index = 0;
		for (auto& pair : pairs) {
			if (index >= (size / sizeof(key_id_pair)) / 8)
				break;
			if ((pair.id & 0xF0000000) == 0 || (pair.id & 0xFFFFFFF0) == 0xFFFFFFF0) {
				std::cerr << "ID at index " << index << " was not overwritten!\n";
			}
			++index;
		}
		pairs.clear();
	}
}
