//

#ifndef IRCHECK_BVH_LAYOUT_H
#define IRCHECK_BVH_LAYOUT_H

#include "bvh.hpp"
#include <../../util/macros.h>
#include <bvh/build_interface.h>
#include <radix_sort/radix_sort_vk.h>
#include <radix_sort/targets/u64/config.h>

#include <vulkan/vk_layer.h>

#include <radix_sort/common/macros.h>
#include <radix_sort/shaders/push.h>


/* TODO: This is from the Mesa source tree and needs to be kept in sync.
 *       Figure out a way to avoid having to manually update things. */

static const struct radix_sort_vk_target_config target_config = {
	.keyval_dwords = RS_KEYVAL_DWORDS,

	.histogram =
		{
			.workgroup_size_log2 = RS_HISTOGRAM_WORKGROUP_SIZE_LOG2,
			.subgroup_size_log2 = RS_HISTOGRAM_SUBGROUP_SIZE_LOG2,
			.block_rows = RS_HISTOGRAM_BLOCK_ROWS,
		},

	.prefix =
		{
			.workgroup_size_log2 = RS_PREFIX_WORKGROUP_SIZE_LOG2,
			.subgroup_size_log2 = RS_PREFIX_SUBGROUP_SIZE_LOG2,
		},

	.scatter =
		{
			.workgroup_size_log2 = RS_SCATTER_WORKGROUP_SIZE_LOG2,
			.subgroup_size_log2 = RS_SCATTER_SUBGROUP_SIZE_LOG2,
			.block_rows = RS_SCATTER_BLOCK_ROWS,
		},
};

void radix_sort_vk_get_memory_requirements(radix_sort_vk_target_config const* config, uint32_t count,
										   radix_sort_vk_memory_requirements_t* mr) {
	//
	// Keyval size
	//
	mr->keyval_size = config->keyval_dwords * sizeof(uint32_t);

	//
	// Subgroup and workgroup sizes
	//
	uint32_t const histo_sg_size = 1 << config->histogram.subgroup_size_log2;
	uint32_t const histo_wg_size = 1 << config->histogram.workgroup_size_log2;
	uint32_t const prefix_sg_size = 1 << config->prefix.subgroup_size_log2;
	uint32_t const scatter_wg_size = 1 << config->scatter.workgroup_size_log2;
	uint32_t const internal_sg_size = MAX_MACRO(uint32_t, histo_sg_size, prefix_sg_size);

	//
	// If for some reason count is zero then initialize appropriately.
	//
	if (count == 0) {
		mr->keyvals_size = 0;
		mr->keyvals_alignment = mr->keyval_size * histo_sg_size;
		mr->internal_size = 0;
		mr->internal_alignment = internal_sg_size * sizeof(uint32_t);
		mr->indirect_size = 0;
		mr->indirect_alignment = internal_sg_size * sizeof(uint32_t);
	} else {
		//
		// Keyvals
		//

		// Round up to the scatter block size.
		//
		// Then round up to the histogram block size.
		//
		// Fill the difference between this new count and the original keyval
		// count.
		//
		// How many scatter blocks?
		//
		uint32_t const scatter_block_kvs = scatter_wg_size * config->scatter.block_rows;
		uint32_t const scatter_blocks = (count + scatter_block_kvs - 1) / scatter_block_kvs;
		uint32_t const count_ru_scatter = scatter_blocks * scatter_block_kvs;

		//
		// How many histogram blocks?
		//
		// Note that it's OK to have more max-valued digits counted by the histogram
		// than sorted by the scatters because the sort is stable.
		//
		uint32_t const histo_block_kvs = histo_wg_size * config->histogram.block_rows;
		uint32_t const histo_blocks = (count_ru_scatter + histo_block_kvs - 1) / histo_block_kvs;
		uint32_t const count_ru_histo = histo_blocks * histo_block_kvs;

		mr->keyvals_size = mr->keyval_size * count_ru_histo;
		mr->keyvals_alignment = mr->keyval_size * histo_sg_size;

		//
		// Internal
		//
		// NOTE: Assumes .histograms are before .partitions.
		//
		// Last scatter workgroup skips writing to a partition.
		//
		// One histogram per (keyval byte + partitions)
		//
		uint32_t const partitions = scatter_blocks - 1;

		mr->internal_size = (mr->keyval_size + partitions) * (RS_RADIX_SIZE * sizeof(uint32_t));
		mr->internal_alignment = internal_sg_size * sizeof(uint32_t);

		//
		// Indirect
		//
		mr->indirect_size = sizeof(struct rs_indirect_info);
		mr->indirect_alignment = sizeof(struct u32vec4);
	}
}

#define MORTON_BIT_SIZE  24

enum radv_accel_struct_build_type {
	RADV_ACCEL_STRUCT_BUILD_TYPE_LBVH,
	RADV_ACCEL_STRUCT_BUILD_TYPE_PLOC,
};

struct acceleration_structure_layout {
	uint32_t bvh_offset;
	uint32_t size;
};

enum internal_build_type {
	INTERNAL_BUILD_TYPE_LBVH,
	INTERNAL_BUILD_TYPE_PLOC,
};

struct build_config {
	enum internal_build_type internal_type;
	bool extended_sah;
	bool compact;
};

static struct build_config
build_config(uint32_t leaf_count, const VkAccelerationStructureBuildGeometryInfoKHR *build_info)
{
	struct build_config config = {};

	if (leaf_count <= 4)
		config.internal_type = INTERNAL_BUILD_TYPE_LBVH;
	else if (build_info->type == VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR)
		config.internal_type = INTERNAL_BUILD_TYPE_LBVH;
	else if (!(build_info->flags & VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_BUILD_BIT_KHR) &&
			 !(build_info->flags & VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR))
		config.internal_type = INTERNAL_BUILD_TYPE_PLOC;
	else
		config.internal_type = INTERNAL_BUILD_TYPE_LBVH;

	/* 4^(lds stack entry count) assuming we push 1 node on average. */
	uint32_t lds_spill_threshold = 1 << (8 * 2);
	if (leaf_count < lds_spill_threshold)
		config.extended_sah = true;

	if (build_info->flags & VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_COMPACTION_BIT_KHR)
		config.compact = true;

	return config;
}

static enum radv_accel_struct_build_type
build_type(uint32_t leaf_count, const VkAccelerationStructureBuildGeometryInfoKHR *build_info)
{
	if (leaf_count <= 4)
		return RADV_ACCEL_STRUCT_BUILD_TYPE_LBVH;

	if (build_info->type == VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR)
		return RADV_ACCEL_STRUCT_BUILD_TYPE_LBVH;

	if (!(build_info->flags & VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_BUILD_BIT_KHR) &&
		!(build_info->flags & VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR))
		return RADV_ACCEL_STRUCT_BUILD_TYPE_PLOC;
	else
		return RADV_ACCEL_STRUCT_BUILD_TYPE_LBVH;
}

/**
 * Align a value up to an alignment value
 *
 * If \c value is not already aligned to the requested alignment value, it
 * will be rounded up.
 *
 * \param value  Value to be rounded
 * \param alignment  Alignment value to be used.  This must be a power of two.
 *
 * \sa ROUND_DOWN_TO()
 */

#if defined(ALIGN)
#undef ALIGN
#endif
static inline uintptr_t ALIGN(uintptr_t value, int32_t alignment) {
	return (((value) + (alignment)-1) & ~((alignment)-1));
}

static void
get_build_layout(uint32_t leaf_count,
				 const VkAccelerationStructureBuildGeometryInfoKHR *build_info,
				 struct acceleration_structure_layout *accel_struct, struct scratch_layout *scratch) {
	uint32_t internal_count = MAX2(leaf_count, 2) - 1;

	VkGeometryTypeKHR geometry_type = VK_GEOMETRY_TYPE_TRIANGLES_KHR;

	if (build_info->geometryCount) {
		if (build_info->pGeometries)
			geometry_type = build_info->pGeometries[0].geometryType;
		else
			geometry_type = build_info->ppGeometries[0]->geometryType;
	}

	uint32_t bvh_leaf_size;
	uint32_t ir_leaf_size;
	switch (geometry_type) {
		case VK_GEOMETRY_TYPE_TRIANGLES_KHR:
			ir_leaf_size = sizeof(struct radv_ir_triangle_node);
			bvh_leaf_size = sizeof(struct radv_bvh_triangle_node);
			break;
		case VK_GEOMETRY_TYPE_AABBS_KHR:
			ir_leaf_size = sizeof(struct radv_ir_aabb_node);
			bvh_leaf_size = sizeof(struct radv_bvh_aabb_node);
			break;
		case VK_GEOMETRY_TYPE_INSTANCES_KHR:
			ir_leaf_size = sizeof(struct radv_ir_instance_node);
			bvh_leaf_size = sizeof(struct radv_bvh_instance_node);
			break;
		default:
			unreachable("Unknown VkGeometryTypeKHR");
	}

	if (accel_struct) {
		uint64_t bvh_size = bvh_leaf_size * leaf_count + sizeof(struct radv_bvh_box32_node) * internal_count;
		uint32_t offset = 0;
		offset += sizeof(struct radv_accel_struct_header);
		/* Parent links, which have to go directly before bvh_offset as we index them using negative
       * offsets from there. */
		offset += bvh_size / 64 * 4;

		/* The BVH and hence bvh_offset needs 64 byte alignment for RT nodes. */
		offset = ALIGN(offset, 64);
		accel_struct->bvh_offset = offset;

		offset += bvh_size;

		accel_struct->size = offset;
	}

	if (scratch) {
		radix_sort_vk_memory_requirements_t requirements = {
			0,
		};
		radix_sort_vk_get_memory_requirements(&target_config, leaf_count, &requirements);

		uint32_t offset = 0;

		uint32_t ploc_scratch_space = 0;
		uint32_t lbvh_node_space = 0;

		struct build_config config = build_config(leaf_count, build_info);

		if (config.internal_type == INTERNAL_BUILD_TYPE_PLOC)
			ploc_scratch_space =
				DIV_ROUND_UP(leaf_count, PLOC_WORKGROUP_SIZE) * sizeof(struct ploc_prefix_scan_partition);
		else
			lbvh_node_space = sizeof(struct lbvh_node_info) * internal_count;

		scratch->header_offset = offset;
		offset += sizeof(struct radv_ir_header);

		scratch->sort_buffer_offset[0] = offset;
		offset += requirements.keyvals_size;

		scratch->sort_buffer_offset[1] = offset;
		offset += requirements.keyvals_size;

		scratch->sort_internal_offset = offset;
		/* Internal sorting data is not needed when PLOC/LBVH are invoked,
       * save space by aliasing them */
		scratch->ploc_prefix_sum_partition_offset = offset;
		scratch->lbvh_node_offset = offset;
		offset += MAX3(requirements.internal_size, ploc_scratch_space, lbvh_node_space);

		scratch->ir_offset = offset;
		offset += ir_leaf_size * leaf_count;

		scratch->internal_node_offset = offset;
		offset += sizeof(struct radv_ir_box_node) * internal_count;

		scratch->size = offset;
	}
}

#endif // IRCHECK_BVH_LAYOUT_H
