/*
 * Copyright © 2024 Valve Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "build_interface.h"
#include <cstring>
#include <exception>
#include <fstream>
#include <iostream>
#include <optional>
#include <vector>

template <typename T> std::optional<T> read(std::ifstream& stream) {
	T t;
	char data[sizeof(T)];
	stream.read(data, sizeof(T));
	if (stream.good()) {
		std::memcpy(&t, data, sizeof(T));
		return t;
	}
	return {};
}

int main(int argc, char** argv) {
	if (argc <= 1) {
		std::cout << "Enter a filename to analyze\n";
		exit(-1);
	}
	std::ifstream file = std::ifstream(argv[1]);
	uint32_t asCount = *read<uint32_t>(file);

	std::vector<uint32_t> asSizes;
	asSizes.reserve(asCount);
	for (uint32_t i = 0; i < asCount; ++i) {
		asSizes.push_back(*read<uint32_t>(file));
	}

	for (uint32_t i = 0; i < asCount; ++i) {
		ploc_scratch_data data = *read<ploc_scratch_data>(file);
		std::vector<ploc_prefix_scan_partition> partitions;
		partitions.reserve(data.debug_internal_node_count);
		for (uint32_t j = 0; j < data.debug_internal_node_count; ++j) {
			partitions.push_back(*read<ploc_prefix_scan_partition>(file));
		}

		uint32_t current_prefix_sum = 0;
		uint32_t index = 0;
		for (auto& partition : partitions) {
			if (data.current_phase_end_counter - data.current_phase_start_counter == index)
				break;
			current_prefix_sum += partition.aggregate;
			if (!partition.inclusive_sum_available && data.debug_internal_node_count != 1) {
				std::cerr << "Partition " << index << " did not finish prefix sum calculation!\n";
			}
			if (partition.inclusive_sum != current_prefix_sum) {
				std::cerr << "Partition " << index << " has a wrong prefix sum: " << partition.inclusive_sum
						  << " instead of the expected " << current_prefix_sum << "!\n";
				break;
			}
			++index;
		}
	}
}
