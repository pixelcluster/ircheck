/*
 * Copyright © 2024 Valve Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */


#include "entry.hpp"
#include <algorithm>
#include <execution>
#include <iostream>
#include <numeric>

#include "validator.hpp"
#include "bvh_layout.h"

/* Various driver utilities to calculate scratch layout. */

uint32_t builds_to_skip = 0;
uint32_t builds_skipped = 0;

extern "C" {
VKAPI_ATTR VkResult VKAPI_CALL ircheck_EnumeratePhysicalDevices(VkInstance instance, uint32_t* pPhysicalDeviceCount,
																VkPhysicalDevice* pPhysicalDevices) {
	std::lock_guard<std::mutex> lock = std::lock_guard<std::mutex>(table_mutex);
	InstanceDispatchTable& table = instanceTables[getKey(instance)];

	VkResult result = table.enumeratePhysicalDevices(instance, pPhysicalDeviceCount, pPhysicalDevices);
	if (result != VK_SUCCESS && result != VK_INCOMPLETE)
		return result;

	if (pPhysicalDevices) {
		for (uint32_t i = 0; i < *pPhysicalDeviceCount; ++i) {
			deviceInstances[pPhysicalDevices[i]] = instance;
		}
	}
	return result;
}

VKAPI_ATTR VkResult VKAPI_CALL ircheck_AllocateMemory(VkDevice device, const VkMemoryAllocateInfo* pAllocateInfo,
													  const VkAllocationCallbacks* pAllocator,
													  VkDeviceMemory* pMemory) {
	bool has_device_address = false;
	for (const void* pNext = pAllocateInfo->pNext; pNext;) {
		const VkBaseInStructure* structure = reinterpret_cast<const VkBaseInStructure*>(pNext);
		if (structure->sType == VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO) {
			if (reinterpret_cast<const VkMemoryAllocateFlagsInfo*>(structure)->flags &
				VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT) {
				has_device_address = true;
			}
		}
		pNext = structure->pNext;
	}

	std::lock_guard<std::mutex> lock = std::lock_guard<std::mutex>(table_mutex);
	DeviceData& data = deviceData[getKey(device)];

	VkMemoryAllocateInfo newAllocateInfo = *pAllocateInfo;
	if (data.types[newAllocateInfo.memoryTypeIndex].propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT && newAllocateInfo.memoryTypeIndex != data.hvvTypeIndex + 1) {
		newAllocateInfo.memoryTypeIndex = data.hvvTypeIndex;
	}

	VkResult result = data.dispatchTable.allocateMemory(device, &newAllocateInfo, pAllocator, pMemory);
	if (result != VK_SUCCESS || !has_device_address)
		return result;
	data.memoryMaps[*pMemory] = { .size = pAllocateInfo->allocationSize };
	return VK_SUCCESS;
}

VKAPI_ATTR VkDeviceAddress VKAPI_CALL ircheck_GetBufferDeviceAddress(VkDevice device,
																	 const VkBufferDeviceAddressInfo* pInfo) {
	std::lock_guard<std::mutex> lock = std::lock_guard<std::mutex>(table_mutex);
	DeviceData& data = deviceData[getKey(device)];

	VkDeviceAddress address = data.dispatchTable.getBDA(device, pInfo);

	BufferBindInfo& bindInfo = data.bindInfos[pInfo->buffer];
	VARange& range = data.memoryMaps[bindInfo.memory];
	range.start = address - bindInfo.offset;
	range.end = range.start + range.size;
	return address;
}

VKAPI_ATTR VkDeviceAddress VKAPI_CALL ircheck_GetBufferDeviceAddressKHR(VkDevice device,
																		const VkBufferDeviceAddressInfoKHR* pInfo) {
	std::lock_guard<std::mutex> lock = std::lock_guard<std::mutex>(table_mutex);
	DeviceData& data = deviceData[getKey(device)];

	VkDeviceAddress address = data.dispatchTable.getBDAKHR(device, pInfo);

	BufferBindInfo& bindInfo = data.bindInfos[pInfo->buffer];
	VARange& range = data.memoryMaps[bindInfo.memory];
	range.start = address - bindInfo.offset;
	range.end = range.start + range.size;
	return address;
}

VKAPI_ATTR VkDeviceAddress VKAPI_CALL ircheck_GetBufferDeviceAddressEXT(VkDevice device,
																		const VkBufferDeviceAddressInfoEXT* pInfo) {
	std::lock_guard<std::mutex> lock = std::lock_guard<std::mutex>(table_mutex);
	DeviceData& data = deviceData[getKey(device)];

	VkDeviceAddress address = data.dispatchTable.getBDAEXT(device, pInfo);

	BufferBindInfo& bindInfo = data.bindInfos[pInfo->buffer];
	VARange& range = data.memoryMaps[bindInfo.memory];
	range.start = address - bindInfo.offset;
	range.end = range.start + range.size;
	return address;
}

VKAPI_ATTR VkResult VKAPI_CALL ircheck_CreateBuffer(VkDevice device, const VkBufferCreateInfo* pCreateInfo,
													const VkAllocationCallbacks* pAllocator, VkBuffer* pBuffer) {
	std::lock_guard<std::mutex> lock = std::lock_guard<std::mutex>(table_mutex);
	DeviceData& data = deviceData[getKey(device)];

	VkResult result = data.dispatchTable.createBuffer(device, pCreateInfo, pAllocator, pBuffer);
	if (result != VK_SUCCESS)
		return result;

	data.bufferSizes[*pBuffer] = pCreateInfo->size;
	return VK_SUCCESS;
}

VKAPI_ATTR VkResult VKAPI_CALL ircheck_BindBufferMemory(VkDevice device, VkBuffer buffer, VkDeviceMemory memory,
														VkDeviceSize memoryOffset) {
	std::lock_guard<std::mutex> lock = std::lock_guard<std::mutex>(table_mutex);
	DeviceData& data = deviceData[getKey(device)];
	auto mapIterator = data.memoryMaps.find(memory);
	if (mapIterator != data.memoryMaps.end()) {
		data.bindInfos[buffer] = { .memory = memory, .offset = memoryOffset };
		mapIterator->second.range.push_back(
			{ .start = memoryOffset, .size = data.bufferSizes[buffer], .buffer = buffer });
	}
	return data.dispatchTable.bindBufferMemory(device, buffer, memory, memoryOffset);
}

VKAPI_ATTR VkResult VKAPI_CALL ircheck_BindBufferMemory2KHR(VkDevice device, uint32_t bindInfoCount,
															const VkBindBufferMemoryInfoKHR* pBindInfos) {
	std::lock_guard<std::mutex> lock = std::lock_guard<std::mutex>(table_mutex);
	DeviceData& data = deviceData[getKey(device)];
	for (uint32_t i = 0; i < bindInfoCount; ++i) {
		const VkBindBufferMemoryInfo& info = pBindInfos[i];
		auto mapIterator = data.memoryMaps.find(info.memory);
		if (mapIterator != data.memoryMaps.end()) {
			data.bindInfos[info.buffer] = { .memory = info.memory, .offset = info.memoryOffset };
			mapIterator->second.range.push_back(
				{ .start = info.memoryOffset, .size = data.bufferSizes[info.buffer], .buffer = info.buffer });
		}
	}
	return data.dispatchTable.bindBufferMemory2KHR(device, bindInfoCount, pBindInfos);
}

VKAPI_ATTR VkResult VKAPI_CALL ircheck_BindBufferMemory2(VkDevice device, uint32_t bindInfoCount,
														 const VkBindBufferMemoryInfo* pBindInfos) {
	std::lock_guard<std::mutex> lock = std::lock_guard<std::mutex>(table_mutex);
	DeviceData& data = deviceData[getKey(device)];
	for (uint32_t i = 0; i < bindInfoCount; ++i) {
		const VkBindBufferMemoryInfo& info = pBindInfos[i];
		auto mapIterator = data.memoryMaps.find(info.memory);
		if (mapIterator != data.memoryMaps.end()) {
			data.bindInfos[info.buffer] = { .memory = info.memory, .offset = info.memoryOffset };
			mapIterator->second.range.push_back(
				{ .start = info.memoryOffset, .size = data.bufferSizes[info.buffer], .buffer = info.buffer });
		}
	}
	return data.dispatchTable.bindBufferMemory2(device, bindInfoCount, pBindInfos);
}

VKAPI_ATTR void VKAPI_CALL ircheck_DestroyBuffer(VkDevice device, VkBuffer buffer,
												 const VkAllocationCallbacks* pAllocator) {
	std::lock_guard<std::mutex> lock = std::lock_guard<std::mutex>(table_mutex);
	DeviceData& data = deviceData[getKey(device)];
	if (data.bindInfos.find(buffer) == data.bindInfos.end())
		goto notbound;
	{
		VkDeviceMemory boundMemory = data.bindInfos[buffer].memory;
		for (uint32_t i = 0; i < data.memoryMaps[boundMemory].range.size(); ++i) {
			if (data.memoryMaps[boundMemory].range[i].buffer == buffer) {
				data.memoryMaps[boundMemory].range.erase(data.memoryMaps[boundMemory].range.begin() + i);
			}
		}
		data.bindInfos.erase(buffer);
	}

notbound:
	data.bufferSizes.erase(buffer);
	data.dispatchTable.destroyBuffer(device, buffer, pAllocator);
}

// yoinked from u_math.h
static inline uint64_t align64(uint64_t value, unsigned alignment) {
	return (value + alignment - 1) & ~((uint64_t)alignment - 1);
}

VKAPI_ATTR void VKAPI_CALL ircheck_CmdBuildAccelerationStructuresKHR(
	VkCommandBuffer buffer, uint32_t infoCount, const VkAccelerationStructureBuildGeometryInfoKHR* pInfos,
	const VkAccelerationStructureBuildRangeInfoKHR* const* ppBuildRangeInfos) {
	std::lock_guard<std::mutex> lock = std::lock_guard<std::mutex>(table_mutex);
	DeviceData& data = deviceData[getKey(buffer)];
	data.dispatchTable.cmdBuildKHR(buffer, infoCount, pInfos, ppBuildRangeInfos);

	if (builds_to_skip > builds_skipped) {
		++builds_skipped;
		return;
	}

	for (uint32_t i = 0; i < infoCount; ++i) {
		VkGeometryTypeKHR geometryType;

		std::vector<uint32_t> primitiveCounts;
		primitiveCounts.reserve(pInfos[i].geometryCount);

		for (uint32_t j = 0; j < pInfos[i].geometryCount; ++j) {
			const VkAccelerationStructureBuildRangeInfoKHR& info = ppBuildRangeInfos[i][j];
			primitiveCounts.push_back(info.primitiveCount);
			const VkAccelerationStructureGeometryKHR& geometry =
				pInfos[i].ppGeometries ? *pInfos[i].ppGeometries[j] : pInfos[i].pGeometries[j];
			geometryType = geometry.geometryType;
		}

		VkAccelerationStructureBuildSizesInfoKHR info;
		data.dispatchTable.getBuildSizes(deviceData[getKey(buffer)].device,
										 VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR, &pInfos[i],
										 primitiveCounts.data(), &info);

		uint32_t leafCount = std::accumulate(primitiveCounts.begin(), primitiveCounts.end(), 0);

		scratch_layout layout;
		get_build_layout(leafCount, &pInfos[i], NULL, &layout);

		VkBuffer scratchBuffer;
		uint64_t bufferOffset;
		VkDeviceMemory scratchMemory;
		uint64_t memoryOffset;
		for (auto& [memory, range] : data.memoryMaps) {
			if (pInfos[i].scratchData.deviceAddress >= range.start &&
				pInfos[i].scratchData.deviceAddress <= range.end) {
				scratchMemory = memory;
				memoryOffset = pInfos[i].scratchData.deviceAddress - range.start;
				for (auto& boundRange : range.range) {
					if (boundRange.start <= memoryOffset && boundRange.start + boundRange.size > memoryOffset) {
						scratchBuffer = boundRange.buffer;
						bufferOffset = memoryOffset - boundRange.start;
					}
				}
			}
		}

		VkBuffer dumpBuf;
		VkBufferCreateInfo dumpBufCreate = { .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
											 .size = layout.size,
											 .usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT };
		data.dispatchTable.createBuffer(data.device, &dumpBufCreate, NULL, &dumpBuf);

		VkDeviceMemory mem;
		VkMemoryAllocateInfo allocInfo = { .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
										   .allocationSize = align64(layout.size, 16),
										   .memoryTypeIndex = data.hvvTypeIndex };
		data.dispatchTable.allocateMemory(data.device, &allocInfo, NULL, &mem);

		void* hostPtr;
		data.dispatchTable.mapMemory(data.device, mem, 0, VK_WHOLE_SIZE, 0, &hostPtr);

		data.dispatchTable.bindBufferMemory(data.device, dumpBuf, mem, 0);

		VkMemoryBarrier fullBar = { .sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER,
									.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT,
									.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT };
		data.dispatchTable.barrier(buffer, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, 0, 1,
								   &fullBar, 0, nullptr, 0, nullptr);
		VkBufferCopy copy = { .srcOffset = bufferOffset, .dstOffset = 0, .size = layout.size };
		data.dispatchTable.copyBuffer(buffer, scratchBuffer, dumpBuf, 1, &copy);

		data.dispatchTable.barrier(buffer, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, 0, 1,
								   &fullBar, 0, nullptr, 0, nullptr);

		data.buildCommandBuffers[buffer].push_back({ .layout = layout,
													 .scratchMem = hostPtr,
													 .buffer = dumpBuf,
													 .devmem = mem,
													 .leafCount = leafCount,
													 .scratchSize = info.buildScratchSize,
													 .structureType = pInfos[i].type,
													 .geometryType = geometryType,
													 .scratchVA = pInfos[i].scratchData.deviceAddress });
	}
}

static std::mutex cerr_mutex;
static std::atomic<uint64_t> as_dump_idx;

void validate_buffer(DeviceData& data, VkCommandBuffer buffer) {
	std::for_each(
		std::execution::par_unseq, data.buildCommandBuffers[buffer].begin(), data.buildCommandBuffers[buffer].end(),
		[&data](const auto& command) {
			Validator validator = Validator(reinterpret_cast<uint8_t*>(command.scratchMem), &command.layout,
											command.geometryType);
			if (!(validator.validateBVH() && validator.validateKeyIDs() && validator.validatePartitions())) {
				uint64_t idx = as_dump_idx.fetch_add(1);
				std::string filename = "/home/pixelcluster/tmp/invalid-scratch-data/data " + std::to_string(idx) + ".bin";
				auto lock = std::lock_guard<std::mutex>(cerr_mutex);
				// clang-format off
						  std::cerr
						    	<< "Invalid BVH detected. Dumping scratch to " << filename << ". Relevant offsets:"
						    	<< "\nHeader at " << command.layout.header_offset
						    	<< "\nSrc ids at " << command.layout.sort_buffer_offset[0]
						    	<< "\nDst ids at " << command.layout.sort_buffer_offset[1]
						    	<< "\nSync state at " << command.layout.sort_internal_offset
						    	<< "\nIR at " << command.layout.ir_offset << "\n";
				// clang-format on
				FILE* file = fopen(filename.c_str(), "wb");
				fwrite(&command.geometryType, sizeof(VkGeometryTypeKHR), 1, file);
				fwrite(&command.layout, sizeof(scratch_layout), 1, file);
				fwrite(command.scratchMem, 1, command.scratchSize, file);
				fclose(file);
			}
			validator.destroy();
			data.dispatchTable.destroyBuffer(data.device, command.buffer, NULL);
			data.dispatchTable.freeMemory(data.device, command.devmem, NULL);
		});
	data.buildCommandBuffers.erase(buffer);
}

VKAPI_ATTR VkResult VKAPI_CALL ircheck_QueueSubmit(VkQueue queue, uint32_t submitCount, const VkSubmitInfo* pSubmits,
												   VkFence fence) {
	std::lock_guard<std::mutex> lock = std::lock_guard<std::mutex>(table_mutex);
	DeviceData& data = deviceData[getKey(queue)];
	bool destroyFence = false;
	if (!fence) {
		VkFenceCreateInfo fenceCreateInfo = {
			.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO
		};
		data.dispatchTable.createFence(data.device, &fenceCreateInfo, nullptr, &fence);
		destroyFence = true;
	}

	VkResult result = data.dispatchTable.queueSubmit(queue, submitCount, pSubmits, fence);
	if (result != VK_SUCCESS)
		return result;

	result = data.dispatchTable.waitForFences(data.device, 1, &fence, VK_TRUE, 3000000000);

	for (uint32_t i = 0; i < submitCount; ++i) {
		for (uint32_t j = 0; j < pSubmits[i].commandBufferCount; ++j) {
			validate_buffer(data, pSubmits[i].pCommandBuffers[j]);
		}
	}

	if (destroyFence) {
		vkDestroyFence(data.device, fence, nullptr);
	}

	return VK_SUCCESS;
}

VKAPI_ATTR VkResult VKAPI_CALL ircheck_QueueSubmit2(VkQueue queue, uint32_t submitCount, const VkSubmitInfo2* pSubmits,
													VkFence fence) {
	std::lock_guard<std::mutex> lock = std::lock_guard<std::mutex>(table_mutex);
	DeviceData& data = deviceData[getKey(queue)];
	bool destroyFence = false;
	if (!fence) {
		VkFenceCreateInfo fenceCreateInfo = {
			.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO
		};
		data.dispatchTable.createFence(data.device, &fenceCreateInfo, nullptr, &fence);
		destroyFence = true;
	}

	VkResult result = data.dispatchTable.queueSubmit2(queue, submitCount, pSubmits, fence);
	if (result != VK_SUCCESS)
		return result;

	result = data.dispatchTable.waitForFences(data.device, 1, &fence, VK_TRUE, 3000000000);

	for (uint32_t i = 0; i < submitCount; ++i) {
		for (uint32_t j = 0; j < pSubmits[i].commandBufferInfoCount; ++j) {
			validate_buffer(data, pSubmits[i].pCommandBufferInfos[j].commandBuffer);
		}
	}

	if (destroyFence) {
		data.dispatchTable.destroyFence(data.device, fence, nullptr);
	}

	return VK_SUCCESS;
}

VKAPI_ATTR VkResult VKAPI_CALL ircheck_QueueSubmit2KHR(VkQueue queue, uint32_t submitCount,
													   const VkSubmitInfo2KHR* pSubmits, VkFence fence) {
	std::lock_guard<std::mutex> lock = std::lock_guard<std::mutex>(table_mutex);
	DeviceData& data = deviceData[getKey(queue)];
	bool destroyFence = false;
	if (!fence) {
		VkFenceCreateInfo fenceCreateInfo = {
			.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO
		};
		data.dispatchTable.createFence(data.device, &fenceCreateInfo, nullptr, &fence);
		destroyFence = true;
	}

	VkResult result = data.dispatchTable.queueSubmit2(queue, submitCount, pSubmits, fence);
	if (result != VK_SUCCESS)
		return result;

	result = data.dispatchTable.waitForFences(data.device, 1, &fence, VK_TRUE, 3000000000);

	for (uint32_t i = 0; i < submitCount; ++i) {
		for (uint32_t j = 0; j < pSubmits[i].commandBufferInfoCount; ++j) {
			validate_buffer(data, pSubmits[i].pCommandBufferInfos[j].commandBuffer);
		}
	}

	if (destroyFence) {
		vkDestroyFence(data.device, fence, nullptr);
	}

	return VK_SUCCESS;
}
}
